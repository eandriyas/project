<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
		$this->load->model('project');
	}
	public function pull(){
		exec('git pull');
	}
	public function index()
	{
		$data['projects'] = $this->project->get_all();

		$this->load->view('home', $data);
	}
	public function baru(){
		$this->load->view('tambah');
	}
	public function tambah(){
		$this->form_validation->set_rules('judul', 'Judul', 'required');
		$this->form_validation->set_rules('link', 'Link', 'required');
		
		if(!empty($_POST)){
			if($this->form_validation->run() === true){
				$data = [
					'judul' => $this->input->post('judul'),
					'link' => $this->input->post('link'),
					'status' => $this->input->post('status'),
					'created_at' => date('Y-m-d H:i:s'),
					'keterangan' => $this->input->post('keterangan')
					];
					$tambah = $this->project->tambah($data);
					redirect('home', 'refresh');
				} else {
					$this->session->set_flashdata('error', validation_errors());
					redirect('home', 'refresh');
				}
		} else{
			redirect('home', 'refresh');
		}
	}
	public function ubah($id){
		if (!empty($_POST)) {
			$data = [
			'judul' => $this->input->post('judul'),
			'link' => $this->input->post('link'),
			'status' => $this->input->post('status'),
			'keterangan' => $this->input->post('keterangan')
			];

			$ubah = $this->project->ubah($id, $data);
			redirect('home', 'refresh');
		}
		$data['project'] = $this->project->get_one($id);
		$this->load->view('ubah', $data);
		

	}
	public function hapus($id){
		$hapus = $this->project->hapus($id);

		if($hapus){
			$this->session->set_flashdata('success', 'Berhasil menghapus');
			redirect('home', 'refresh');
		}
	}
}
