<?php 



/**
* 
*/
class Project extends CI_Model
{
	
	
	function tambah($data){
		$query = $this->db->insert('project', $data);
		return $query;
	}
	function get_all(){
		$this->db->from('project');
		$query = $this->db->get();

		return $query->result();
	}
	function get_one($id){
		$this->db->from('project');
		$this->db->where('id', $id);
		$query = $this->db->get();

		return $query->row();
	}
	function ubah($id, $data){
		$this->db->where('id', $id);
		$query = $this->db->update('project', $data);
		return $query;
	}
	function hapus($id){
		$this->db->from('project');
		$this->db->where('id', $id);
		$query = $this->db->delete();

		return $query;
	}

}