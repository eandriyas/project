<?php $this->load->view('header') ?>

		<div class="col-md-5">
			<form action="<?php echo current_url() ?>" method="post" role="form">
				<div class="form-group">
					<label for="">Judul</label>
					<input type="text" class="form-control" name="judul" placeholder="Judul project" value="<?php echo $project->judul; ?>">

				</div>
				<div class="form-group">
					<label for="">Link</label>
					<textarea name="link" id="" cols="30" rows="1" class="form-control"><?php echo $project->link; ?></textarea>
				</div>
				<div class="form-group">
					<label for="">Status</label>
					<select name="status" class="form-control" id="">
					<option value="<?php echo $project->status; ?>"><?php echo ucwords($project->status); ?></option>
						<option value="baru">Baru</option>
						<option value="proses">Proses</option>
						<option value="selesai">Selesai</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" cols="30" rows="10" class="form-control"><?php echo $project->keterangan; ?>  </textarea>
				</div>
				<hr>
				<div class="form-group">
					<input type="submit" value="Ubah" class="btn btn-primary">
				</div>
			</form>
		</div>
		
	</div>
</div>

<script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/sweet/sweetalert-dev.js') ?>"></script>
<script>
$(document).ready(function(){
	$('.delete').on('click', function(){
		var href = $(this).attr('href');
		swal({   
			title: "Apakah anda yakin?",   
			text: "Kamu tidak akan bisa mengembalikan data ini lagi",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Ya, hapus saja!",   
			closeOnConfirm: false 
		}, 
			function(){   
				swal("Berhasil menghapus!", "Data anda berhasil dihapus.", "success"); 
				window.location.replace(href);
			});

		return false;
	});
})
	
</script>

	
</body>
</html>