<?php $this->load->view('header') ?>
		<div class="col-md-5">
			<form action="<?php echo base_url('home/tambah') ?>" method="post" role="form">
				<div class="form-group">
					<label for="">Judul</label>
					<input type="text" class="form-control" name="judul" placeholder="Judul project">

				</div>
				<div class="form-group">
					<label for="">Link</label>
					<textarea name="link" id="" cols="30" rows="1" class="form-control">http://</textarea>
				</div>
				<div class="form-group">
					<label for="">Status</label>
					<select name="status" class="form-control" id="">
						<option value="baru">Baru</option>
						<option value="proses">Proses</option>
						<option value="selesai">Selesai</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" cols="30" rows="10" class="form-control">letak folder asli :  </textarea>
				</div>
				<hr>
				<div class="form-group">
					<input type="submit" value="Tambah" class="btn btn-primary">
				</div>
			</form>
		</div>
	
	
	</div>
	
</div>

<script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/sweet/sweetalert-dev.js') ?>"></script>
<script>
$(document).ready(function(){
	$('.delete').on('click', function(){
		var href = $(this).attr('href');
		swal({   
			title: "Apakah anda yakin?",   
			text: "Kamu tidak akan bisa mengembalikan data ini lagi",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Ya, hapus saja!",   
			closeOnConfirm: false 
		}, 
			function(){   
				swal("Berhasil menghapus!", "Data anda berhasil dihapus.", "success"); 
				window.location.replace(href);
			});

		return false;
	});
})
	
</script>

	
</body>
</html>