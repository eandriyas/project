<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Daftar project</title>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/sweet/sweetalert.css') ?>">
</head>
<body>
<h2>Development version.</h2>
<div class="container-fluid">
	<div class="row">
		<div class="page-header">
		  <h1 style="text-align:center;">Daftar project <small>for AESP</small> </h1>
		</div>



		<div class="col-md-12">
		<?php foreach ($projects as $project): ?>
			<div class="col-sm-6 col-md-2">
			    <div class="thumbnail" style="min-height:450px;">
			      <img src="<?php echo base_url(); ?>/assets/image/thumb.jpg" alt="...">
			      <div class="caption">
			        <h3 style="height:30px;"><?php echo ucwords($project->judul) ?></h3>
			        <p style="min-height:60px;"><?php echo $project->keterangan; ?></p>
			        <p>
			        	<a href="<?php echo $project->link;  ?>" class="btn btn-default" target="_blank">Goto</a>
						<a href="<?php echo base_url('home/ubah/'.$project->id); ?>" class="btn btn-primary">Ubah</a>
						<a href="<?php echo base_url('home/hapus/'.$project->id); ?>" onclick="return false;" class="btn btn-danger delete">Hapus</a>

			        </p>
			      </div>
			    </div>
			</div>
		<?php endforeach; ?>
			<div class="col-sm-6 col-md-2">
			    <div class="thumbnail" style="min-height:400px;text-align:center;">
			     	<button  style="margin-top:190px;" class="btn btn-primary" data-toggle="modal" data-target=".tambah" >Tambah</button>
			    </div>
			</div>
		</div>
	</div>

</div>

<div class="modal fade tambah" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Tambah project</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo base_url('home/tambah') ?>" method="post" role="form">
				<div class="form-group">
					<label for="">Judul</label>
					<input type="text" class="form-control" name="judul" placeholder="Judul project" required>

				</div>
				<div class="form-group">
					<label for="">Link</label>
					<textarea name="link" id="" cols="30" rows="1" class="form-control" required>http://</textarea>
				</div>
				<div class="form-group">
					<label for="">Status</label>
					<select name="status" class="form-control" id="">
						<option value="baru">Baru</option>
						<option value="proses">Proses</option>
						<option value="selesai">Selesai</option>
					</select>
				</div>
				<div class="form-group">
					<label for="">Keterangan</label>
					<textarea name="keterangan" id="" cols="30" rows="10" class="form-control">letak folder asli :  </textarea>
				</div>



			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<input type="submit" value="Tambah" class="btn btn-primary">
			</div>
			</form>
		</div>
	</div>
</div>


<script src="<?php echo base_url('assets/js/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/sweet/sweetalert-dev.js') ?>"></script>
<script>
$(document).ready(function(){
	$('.delete').on('click', function(){
		var href = $(this).attr('href');
		swal({
			title: "Apakah anda yakin?",
			text: "Kamu tidak akan bisa mengembalikan data ini lagi",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Ya, hapus saja!",
			closeOnConfirm: false
		},
			function(){
				swal("Berhasil menghapus!", "Data anda berhasil dihapus.", "success");
				window.location.replace(href);
			});

		return false;
	});
})

</script>


</body>
</html>
