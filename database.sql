/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.8-MariaDB : Database - project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `project`;

/*Table structure for table `project` */

DROP TABLE IF EXISTS `project`;

CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `link` text,
  `keterangan` text,
  `status` enum('baru','proses','selesai') DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `project` */

LOCK TABLES `project` WRITE;

insert  into `project`(`id`,`judul`,`link`,`keterangan`,`status`,`created_at`) values (1,'Dinas tasik','http://prototype.dev','  letak folder asli :  http://localhost/tasik','baru','2016-01-17 01:23:45'),(2,'Uii event','http://uii.dev','  letak folder asli :  http://localhost/uii-event','baru','2016-01-17 01:23:48'),(3,'Ihsan ','http://ihsan.dev','    letak folder asli :  http://localhost/ihsan/sistem-agenda-ihsan','baru','2016-01-17 01:23:50'),(4,'Wordpress development','http://wp.dev',NULL,'baru','2016-01-17 01:23:51'),(6,'Personal project','http://personal.dev','    letak folder asli :  http://localhost/static/personal','baru','2016-01-17 01:23:53');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
